import requests
from fabric.api import local
import fcntl, socket, struct


def get_mac():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', 'eth0'))
    mac = '-'.join(['%02x' % ord(char) for char in info[18:24]])
    return mac.lower()


def upgrade(macaddr):

    macaddr = get_mac()
    result = local("git show HEAD", capture=True)
    new_head = result.split('\n')[0].split(' ')[1]
    requests.get("http://controme-main.appspot.com/get/update/%s/?ver=%s" % (macaddr, new_head))
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")

    err = []

    try:
        local("rm /home/pi/mischer/www/brgwconfiguration/migrations/*")
    except:
        try:
            local("mkdir /home/pi/mischer/www/brgwconfiguration/migrations/")
        except:
            pass

    try:
        result = local("cat /etc/nginx/nginx.conf | grep 'mischerkonfiguration'", capture=True)
        try:
            local("sudo cp /home/pi/mischer/config/nginx.conf /etc/nginx/")
            local("sudo service nginx restart")
        except:
            err.append("nginx konnte nicht neu gestartet werden")
    except:
        pass

    try:
        local("cp -r /home/pi/mischer/www/brgwconfiguration/_migrations/* /home/pi/mischer/www/brgwconfiguration/migrations/")
    except:
        pass

    try:
        result = local("ls /home/pi/mischer/www/db.sqlite3")
    except:
        try:
            local("python /home/pi/mischer/www/manage.py migrate")
        except:
            err.append("db konnte nicht angelegt werden")
    else:
        # do something
        pass

    try:
        res = local("ls -al /etc/ntp.conf", capture=True)
        if 'root root' in res:
            local("sudo chown ntp:ntp /etc/ntp.conf")
        local("sudo service ntp restart")
    except Exception as e:
        err.append("%s bei ntp" % str(e))

    if len(err):
        try:
            from aeshelper import aes_encrypt
            import json
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps({"error": err})))
        except:
            pass
