from fabric.state import env
from fabric.api import local, lcd, settings
import requests
import time
from datetime import datetime
import json
from aeshelper import aes_encrypt


env.hosts = ["localhost"]
path = "/home/pi/mischer"
server = "http://controme-main.appspot.com"


def get_mac():
    with open('/sys/class/net/eth0/address') as f:
        _mac = f.read()
        return _mac.strip().replace(':', '-').lower()


def update(update_override=False):
    macaddr = get_mac()

    if not update_override:
        try:
            uptime = local("cat /proc/uptime", capture=True)
            uptime = uptime.split(' ')[0]
            uptime = float(uptime)
        except Exception as e:
            uptime = None
        data = {'uptime': uptime}
        try:
            rev = local("cat /proc/cpuinfo | grep '^Revision'", capture=True)
            data['rpimodel'] = rev.split(':')[1].strip()
        except:
            pass
        try:
            t = local("cat /sys/class/thermal/thermal_zone0/temp", capture=True)
            data['temp'] = int(t)/1000
        except:
            pass
        try:
            r = requests.post("%s/get/update/%s/" % (server, macaddr), data="data=%s" % aes_encrypt(json.dumps(data)))
        except Exception as e:
            pass

    try:
        if update_override:
            r = requests.get("%s/get/update/%s/?mu" % (server, macaddr))
        else:
            r = requests.get("%s/get/update/%s/" % (server, macaddr))
    except:
        return

    if r.status_code == 200:
        with lcd(path) and settings(warn_only=True):
            result = local("git pull", capture=True)
            if result.return_code != 0:
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps({"git pull error": result.return_code})))
                if update_override:
                    local("touch /home/pi/mischer/failed_pull")

            else:
                local("fab -f /home/pi/mischer/services/upgrade.py upgrade:None")
                try:
                    local("rm /home/pi/mischer/failed_pull")
                except:
                    pass

    else:
        res = local("git diff --stat", capture=True)
        if len(res):
            files = res.split('\n')
            for f in files[:-1]:
                _file = f.split('|')[0].strip()
                local("git checkout %s" % _file)

        res = local("ntpq -p", capture=True)
        if "No association ID" in res:
            local("sudo /etc/init.d/ntp restart")


def revert():

    # todo this'll kill us in any case, right?
    # todo can we fork? copy the below code in a separate file, move it to ~/  and execute it...
    # todo oder kann ein "fatal: loose object" immer von git gc aufgeraeumt werden? nein, oder?
    # todo oder ist das ueberhaupt ein problem? weil wenn es ein git pull nicht verhindert, why should we care?

    print "trying to revert ..."
    with lcd("/home/pi/"):
        local("cp mischer/services/aeskey.py .")
        local("cp mischer/www/brgw/secret_key.py .")
        local("cp mischer/www/db.sqlite3 .")
        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf stop all")
        local("rm -rf mischer")
        local("git clone https://hcerny@bitbucket.org/hcerny/mischer.git mischer/")
        local("mv aeskey.py mischer/services/")
        local("mv secret_key.py mischer/www/brgw/")
        local("mv db.sqlite3 mischer/www/")
        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf start all")
    return True
    pass


def run_tests():
    # eventuell vorhandene unit tests ausfuehren
    pass


if __name__ == "__main__":
    try:
        mac = get_mac()
        tsleep = 60 + 60*(int(mac[-2:], 16) % 8) + (int(mac[-5:-3], 16) % 60)
        tsleep /= 60
    except:
        tsleep = 0

    while True:
        if datetime.now().minute in [tsleep, 10 + tsleep, 20 + tsleep, 30 + tsleep, 40 + tsleep, 50 + tsleep]:
            try:
                update()
                time.sleep(500)
            except Exception:
                import sys, traceback, json

                try:
                    typ, value, trace = sys.exc_info()
                    ret = {
                        'location': 'services/update.py',
                        'exc_type': typ.__name__,
                        'exc_msg': typ.__doc__,
                        'exc_value': str(value).translate(None, "'").translate(None, '"'),
                        'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
                    }
                    err = aes_encrypt(json.dumps(ret))
                    macaddr = get_mac()
                    requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
                except:
                    pass
        time.sleep(30)
