import time
import requests
import json
from fabric.api import local


with open('/sys/class/net/eth0/address') as f:
    mac = f.read()
    macaddr = mac.strip().replace(':', '-').lower()

while True:
    try:

        ifc = local("hostname -I", capture=True)
        own_ip = ifc.strip().split(' ')[0]
        subnets = {own_ip.rsplit('.', 1)[0]}

        resp = requests.get("http://localhost/json/v1/miniservers/")
        if resp.status_code == 200:
            try:
                miniservers = json.loads(resp.content)
            except ValueError:
                print "valueerror"
                miniservers = []
        else:
            miniservers = []

        all_online = True
        for miniserver in miniservers:
            for name, ip in miniserver.items():
                if ip == own_ip:
                    all_online = False
                    break
                subnets.add(ip.rsplit('.', 1)[0])
                try:
                    res = local("ping -c1 %s" % ip, capture=True)  # ping ist natuerlich nicht ausreichend
                    arp = local("arp -a | grep %s" % ip, capture=True)
                    if name.replace('-', ':') not in arp or name.replace('-', ':').upper() not in arp:
                        all_online = False
                except:
                    all_online = False

        hosts = {}

        if all_online and len(miniservers):
            time.sleep(60)
            continue

        for subnet in subnets:
            res = local("sudo nmap -n -sP %s.*" % subnet, capture=True)
            r = res.split('\n')
            ip = mac = None
            for i, line in enumerate(r):
                if line.startswith("Nmap scan report for"):
                    ip = line.rsplit(' ', 1)[1]
                    mac = r[i+2].split(' ')[2]
                    if mac.count(':') != 5: continue
                    hosts[mac.lower().replace(':', '-')] = ip

        ret = {}
        for mac, ip in hosts.items():

            try:
                resp = requests.get("http://%s/get/mixer/%s/config/" % (ip, macaddr), timeout=2)
                if resp.status_code == 200:
                    r = json.loads(resp.content)
                    if macaddr.lower() in resp.content or isinstance(r, dict):
                        ret[mac] = ip
                        break
            except:
                pass

            try:
                resp = requests.get("http://%s/get/differentialcontrol/%s/config/" % (ip, macaddr), timeout=2)
                if resp.status_code == 200:
                    r = json.loads(resp.content)
                    if macaddr.lower() in resp.content or isinstance(r, dict):
                        ret[mac] = ip
                        break
            except:
                pass

        requests.post("http://localhost/json/v1/miniservers/", json=ret)

        time.sleep(60)

    except Exception as e:
        import sys, traceback, json
        try:
            typ, value, trace = sys.exc_info()
            ret = {
                'location': 'services/ipupdater.py',
                'exc_type': typ.__name__,
                'exc_msg': typ.__doc__,
                'exc_value': str(value).translate(None, "'").translate(None, '"'),
                'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
            }
            enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
            err = enc
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
        except:
            pass
