# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
import jsonapi.views as api
import logging
import json
from fabric.api import local, lcd
from datetime import datetime, timedelta
import requests
import fcntl, socket, struct
from django.http import HttpResponse
import pytz
from itertools import chain
import math


def get_mac():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', 'eth0'))
    mac = '-'.join(['%02x' % ord(char) for char in info[18:24]])
    return mac.lower()


def index(request):

    _status = json.loads(api.status(request).content)
    mrs = json.loads(api.mixers(request).content)
    drs = json.loads(api.differentialcontrols(request).content)

    for dr in drs:
        did = dr.keys()[0]
        dr[did]['cc_id'] = did
        for state in _status:
            if int(did) == state.get('cc_id'):
                dr[did]['state'] = state

    for mr in mrs:
        mid = mr.keys()[0]
        mr[mid]['mixer_id'] = mid
        for state in _status:
            if int(mid) == state.get('mixer_id'):
                mr[mid]['state'] = state

    logging.warning("_status: %s" % _status)
    logging.warning("drs: %s" % drs)
    logging.warning("mrs: %s" % mrs)

    return render(request, "index.html", {"status": _status, "regs": list(chain(sorted(drs, key=lambda x: int(x.values()[0]['name'])), sorted(mrs, key=lambda x: int(x.values()[0]['name'])))), "halflen": math.ceil((len(mrs)+len(drs))/2.0)})


def sensors(request):

    _sensors = json.loads(api.sensors(request).content)
    liniendict_hrgw = {
        -1: "-",
        0: "-", 1: "1", 2: "-", 3: "-", 4: "2", 5: "3", 6: "4", 7: "5"
    }
    for i, sensor in enumerate(_sensors):
        _sensors[i][sensor.keys()[0]]['linie'] = liniendict_hrgw[sensor.values()[0]['linie']]

    return render(request, "sensors.html", {"sensors": _sensors})


def inputs(request):
    _response = api.inputs(request).content
    _inputs = json.loads(_response)
    return render(request, "inputs.html", {"inputs": _inputs})


def _get_logs(typ, reg_id):
    try:
        log = local("grep '###status###%s,%s' /var/log/uwsgi/uwsgi.log.1" % (typ, reg_id), capture=True)
    except:
        log = ''
    try:
        log1 = local("grep '###status###%s,%s' /var/log/uwsgi/uwsgi.log" % (typ, reg_id), capture=True)
        log += '\n' + log1
    except:
        pass

    return log


def drs(request, dregid=None):

    if dregid is not None:

        dr = json.loads(api.differentialcontrols(request, dregid).content)
        log_status = _get_logs("dr", dr[0].values()[0]['name'])
        data = {'s1': [], 's2': [], 's3': []}
        for line in log_status.split('\n'):
            try:
                l = line.split(',')
                ts = l[-1].strip()
                data['s3'].append([ts, l[-3]]) if l[-3] != "None" else None
                data['s2'].append([ts, l[-4]]) if l[-4] != "None" else None
                data['s1'].append([ts, l[-5]]) if l[-5] != "None" else None
            except IndexError:
                continue

        if dr[0].values()[0]['s1'] is None:
            del data['s1']
        if dr[0].values()[0]['s3'] is None:
            del data['s3']

        return render(request, "dr.html", {"dr": dr, "data": data})

    else:
        _status = json.loads(api.status(request).content)
        drs = json.loads(api.differentialcontrols(request).content)

        for dr in drs:
            did = dr.keys()[0]
            dr[did]['cc_id'] = did
            for state in _status:
                if int(did) == state.get('cc_id'):
                    dr[did]['state'] = state

        return render(request, "drs.html", {"status": _status, "regs": sorted(drs, key=lambda x: int(x.values()[0]['name'])), "halflen": math.ceil(len(drs)/2.0)})


def mrs(request, mid=None):

    if mid is not None:

        mr = json.loads(api.mixers(request, mid).content)

        log_status = _get_logs("mr", mr[0].values()[0]['name'])
        data = {'setpoint': [], 'outflow': [], 'inflow_hot': [], 'inflow_cold': [], 'position': []}
        for line in log_status.split('\n'):
            try:
                l = line.split(',')
                ts = l[-1].strip()
                data['position'].append([ts, l[-2]]) if l[-2] != "None" else None
                data['setpoint'].append([ts, l[-3]]) if l[-3] != "None" else None
                data['outflow'].append([ts, l[-4]]) if l[-4] != "None" else None
                data['inflow_hot'].append([ts, l[-5]]) if l[-5] != "None" else None
                data['inflow_cold'].append([ts, l[-6]]) if l[-6] != "None" else None
            except IndexError as e:
                continue

        vtrtype = mr[0].values()[0]['vtrtype']
        if vtrtype == 2:
            del data['outflow']
        if vtrtype == 1:
            del data['inflow_cold']
            del data['inflow_cold']

        return render(request, "mr.html", {"mr": mr, "data": data})

    else:
        _status = json.loads(api.status(request).content)
        mrs = json.loads(api.mixers(request).content)

        for mr in mrs:
            mid = mr.keys()[0]
            mr[mid]['mixer_id'] = mid
            for state in _status:
                if int(mid) == state.get('mixer_id'):
                    mr[mid]['state'] = state

        return render(request, "mrs.html", {"status": _status, "regs": sorted(mrs, key=lambda x: int(x.values()[0]['name'])), "halflen": math.ceil(len(mrs)/2.0)})


def otherouts(request):

    modtrans = {'rlr': u'Rücklaufregelung', 'zpr': 'Zweipunktregelung', 'fps': 'FPS', 'pl': 'Raumanforderung'}

    outs = json.loads(api.otherouts(request).content)
    ret = {}
    for mod, vals in outs.items():
        for ausgangno, val in vals.items():
            ret[ausgangno] = (val, modtrans[mod])

    return render(request, "otherouts.html", {"data": sorted(ret.items())})


def get_uptime():

    try:
        uptime = local("cat /proc/uptime", capture=True)
        # uptime = "1234 5678"  # TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uptime = uptime.split(' ')[0]
        uptime = float(uptime)
        uptime = datetime.now(pytz.timezone("Europe/Berlin")) - timedelta(seconds=uptime)
        uptime = uptime.strftime("%H:%M %d.%m.%Y")
    except Exception as e:
        logging.exception("error retrieving uptime")
        uptime = None

    return uptime


def settings(request):

    if request.method == "GET":

        if 'update' in request.GET:
            logging.warning("update")
            macaddr = get_mac()
            res = local("git log -1 --date=raw", capture=True)
            luiso = res.split('\n')[2][8:-6]
            try:
                r = requests.get("http://controme-main.appspot.com/get/update/%s/?hrgwverdate=%s" % (macaddr, luiso))
                if r.status_code == 200:
                    with lcd("/home/pi/mischer/"):
                        local("fab -f /home/pi/mischer/services/update.py update:True")
            except requests.ConnectionError:
                local("touch /home/pi/mischer/failed_pull")
            return HttpResponse("")

        elif 'verdate' in request.GET:
            logging.warning("verdate")
            macaddr = get_mac()
            res = local("git log -1 --date=raw", capture=True)
            luiso = res.split('\n')[2][8:-6]
            r = requests.get("http://controme-main.appspot.com/get/update/%s/?hrgwverdate=%s" % (macaddr, luiso))
            return HttpResponse(status=r.status_code)

        elif '1wreset' in request.GET:
            local("sudo python /home/pi/mischer/hwi/gpio13hilo.py")

        else:
            uptime = get_uptime()
            return render(request, "settings.html", {'uptime': uptime})

    elif request.method == "POST":

        if request.POST.get("code", "").lower() != get_mac():
            uptime = get_uptime()
            return render(request, "settings.html", {'uptime': uptime, 'error': "Falsche Mac-Adresse"})

        if 'reboot' in request.POST.keys():
            try:
                requests.get("http://controme-main.appspot.com/get/update/%s/?reboot" % get_mac())
            except Exception as e:
                logging.exception("fehler in reboot request an gae")

            local("sudo reboot")

        elif 'shutdown' in request.POST.keys():
            try:
                requests.get("http://controme-main.appspot.com/get/update/%s/?reboot" % get_mac())
            except Exception as e:
                logging.exception("fehler in shutdown request an gae")

            local("sudo shutdown -h now")

        elif 'reset' in request.POST.keys():
            try:
                local("mv /home/pi/mischer/www/db.sqlite3 /home/pi/mischer/www/db.old")
                with lcd("/home/pi/mischer/www/"):
                    local("python /home/pi/mischer/www/manage.py migrate")
                local("echo 'flush_all' | nc localhost 11211")
                local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")
            except Exception as e:
                logging.exception("exception executing reset")
                local("mv /Home/pi/mischer/www/db.old /home/pi/mischer/www/db.sqlite3")

        elif 'reffahrt' in request.POST.keys():
            try:
                local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart mischersteuerung")
            except:
                logging.exception("fehler bei der referenzfahrt")
            return redirect("/settings/")

        return HttpResponse("ok")
