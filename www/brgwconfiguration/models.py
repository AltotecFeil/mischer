# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from caching.base import CachingManager, CachingMixin
import logging
from django.core.cache import cache as memcache
import math
from datetime import date, datetime, timedelta
import ast
import pytz
import calendar
import time

try:
    from Crypto.PublicKey import RSA
    from Crypto.Cipher import PKCS1_OAEP, AES
    from Crypto import Random
except ImportError:
    logging.error("couldn't import pycrypto")


def cache_state(func):

    def check_cache(self):
        ls = memcache.get('%s%sls' % (type(self).__name__, self.pk))
        berlin = pytz.timezone("Europe/Berlin")
        if ls:
            ls = berlin.localize(datetime.strptime(ls, "%Y-%m-%d %H:%M:%S"))
            if datetime.now(berlin) < ls + timedelta(seconds=self.scan_interval):
                status = memcache.get('%s%sstatus' % (type(self).__name__, self.pk))

                # status ist nur im cache, wenn outputcontrol.py was geschickt hat
                # dementsprechend wird hier immer neu berechnet, wenn kein status vorhanden

                if status:
                    return status['position']

        ret = func(self)

        memcache.set('%s%slv' % (type(self).__name__, self.pk), ret)
        memcache.set('%s%sls' % (type(self).__name__, self.pk), datetime.now(berlin).strftime("%Y-%m-%d %H:%M:%S"))
        return ret

    return check_cache


class CryptKeys(CachingMixin, models.Model):
    aeskey = models.CharField(blank=True, null=True, max_length=32)

    objects = CachingManager()

    def encrypt(self, data):
        import base64

        iv = Random.new().read(AES.block_size)
        aes = AES.new(base64.b64decode(self.aeskey), AES.MODE_CBC, iv)
        out = iv + aes.encrypt(data + Random.new().read(-len(data) % 16).encode('hex')[::2])

        return '~' + base64.b64encode(out)

    def decrypt(self, data):
        import base64
        data = base64.b64decode(data)
        iv = data[:16]

        aes = AES.new(base64.b64decode(self.aeskey), AES.MODE_CBC, iv)
        data = aes.decrypt(data[16:])

        return data

    class Meta:
        app_label = 'brgwconfiguration'


class RPi(CachingMixin, models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=128)

    # crypt_keys = models.ForeignKey(CryptKeys, null=True)
    # verification_code = models.CharField(max_length=16)
    ip = models.GenericIPAddressField(null=True)

    objects = CachingManager()

    class Meta:
        app_label = 'brgwconfiguration'


class BoilerRoomGateway(CachingMixin, models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=128)

    crypt_keys = models.ForeignKey(CryptKeys, null=True)  # for communicating with the cloud server
    parameters = models.TextField()

    objects = CachingManager()

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except (SyntaxError, ValueError):
            logging.exception("brgw.get_params")
            self.set_parameters({})
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()
        return True

    class Meta:
        app_label = 'brgwconfiguration'


class BRGWInput(CachingMixin, models.Model):
    name = models.CharField(max_length=128)
    brgw = models.ForeignKey(BoilerRoomGateway)

    pins = models.CharField(max_length=32)
    parameters = models.TextField()

    objects = CachingManager()

    def set_value(self, value):
        memcache.set(self.name, value, 900)

    def get_value(self):
        return memcache.get(self.name)

    class Meta:
        app_label = 'brgwconfiguration'


class Sensor(CachingMixin, models.Model):
    name = models.CharField(max_length=23)
    linie = models.IntegerField(default=-1)

    brgw = models.ForeignKey(BoilerRoomGateway, null=True)

    objects = CachingManager()

    def set_value(self, value):
        memcache.set(self.name, value, 900)

    def get_value(self):
        if self.name.startswith("00"):
            try:
                val = float(self.name[-5:].replace('_', '.'))
            except Exception:
                logging.exception("sensor.get_value")
                val = None
            return val
        offset = memcache.get("offset_%s" % self.name)
        val = memcache.get(self.name)
        if val is None:
            return val
        if offset is not None:
            val += offset
        # if val == 151 and self.name == "pt1000":
        #     val = None
        return val

    class Meta:
        app_label = 'brgwconfiguration'


class HeatingCurve(CachingMixin, models.Model):
    name = models.CharField(max_length=32)

    bend = models.FloatField()
    slope = models.FloatField()
    offset = models.FloatField()

    # outside_sensor = models.ForeignKey(Sensor, on_delete=models.SET_NULL, null=True, blank=True)
    outside_sensor = models.ForeignKey(Sensor, models.SET_NULL, null=True, blank=True)

    objects = CachingManager()

    def get_value(self):
        t = self.get_temperature()
        if t > 20:
            t = 20
        if t < -20:
            t = -20
        t = math.fabs(t-20)  # verschiebung analog zum diagram

        return self.offset + (6.0 - self.slope) * math.sqrt(t**self.bend)

    def get_future_value(self, hours):
        # TODO: use weather forecast to calculate
        pass

    def get_temperature(self):
        t = None
        if self.outside_sensor:
            t = self.outside_sensor.get_value()
        elif not self.outside_sensor_id:
            t = memcache.get("atsvalue")
        if t is None:
            t = self.get_fallback_temperature()
        return t

    def get_fallback_temperature(self):
        # TODO: time of day?
        min_avgs = [-2.8, -2.4, 0.2, 3.0, 7.2, 10.5, 12.3, 12.2, 9.6, 5.9, 1.7, -1.3]
        # TODO: max_avgs = [2.1, 3.5, 7.4, 11.7, 16.8, 20.0, 21.8, 21.7, 18.3, 13.2, 7.0, 3.4]
        return min_avgs[date.today().month-1]

    class Meta:
        app_label = 'brgwconfiguration'


class BRGWOutput(CachingMixin, models.Model):
    # logical output bundling one or more pins
    name = models.CharField(max_length=128)
    brgw = models.ForeignKey(BoilerRoomGateway)

    parameters = models.TextField()

    objects = CachingManager()

    def get_analogpins(self):
#        return self.brgwpin.filter(pin_type='ana')
        return BRGWPin.objects.filter(brgwoutput_id=self.pk, pin_type='ana')
    analogpins = property(get_analogpins)

    def get_digitalpins(self):
        # return self.brgwpin.filter(pin_type='dig')
        return BRGWPin.objects.filter(brgwoutput_id=self.pk, pin_type='dig')
    digitalpins = property(get_digitalpins)

    def get_digital_on(self):
        # return self.brgwpin.filter(pin_type='dig_on')
        return BRGWPin.objects.filter(brgwoutput_id=self.pk, pin_type='dig_on')
    digital_on = property(get_digital_on)

    def get_digital_off(self):
        # return self.brgwpin.filter(pin_type='dig_off')
        return BRGWPin.objects.filter(brgwoutput_id=self.pk, pin_type='dig_off')
    digital_off = property(get_digital_off)

    class Meta:
        app_label = 'brgwconfiguration'


class BRGWPin(CachingMixin, models.Model):
    name = models.CharField(max_length=32, blank=True)
    dependent_pin = models.IntegerField(default=0)
    number = models.IntegerField()

    TYPE_CHOICES = (
        ('ana', 'analog'),
        ('dig', 'digital'),
        ('dig_on', 'digital on'),
        ('dig_off', 'digital off'),
    )
    pin_type = models.CharField(choices=TYPE_CHOICES, max_length=7)

    brgwoutput = models.ForeignKey(BRGWOutput)

    parameters = models.TextField()

    objects = CachingManager()

    class Meta:
        app_label = 'brgwconfiguration'


class ControlEntity(models.Model):
    master = models.CharField(max_length=17)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=128)
    control_algorithm = models.CharField(max_length=32)
    scan_interval = models.IntegerField()

    brgwoutput = models.ForeignKey(BRGWOutput, on_delete=models.CASCADE, null=True, blank=True)

    parameters = models.TextField()

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except (SyntaxError, ValueError):
            logging.exception("ce.get_params")
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()
        return True

    def get_output_state(self):
        # this returns a value in [0;100]. brgwpin appropriately translates the value.
        raise NotImplementedError()

    def get_last_value(self):
        return memcache.get('%s%slv' % (type(self).__name__, self.pk))

    class Meta:
        app_label = 'brgwconfiguration'
        abstract = True


class Mixer(CachingMixin, ControlEntity):

    runtime = models.IntegerField()

    objects = CachingManager()

    @cache_state
    def get_output_state(self):
        params = self.get_parameters()
        outflow = params['outflow']
        vtrtype = params.get('vtrtype', '3')
        controller = params.get('controller', 'normal')
        min_voltage = params.get('min_voltage', 0) * 10.0
        max_voltage = params.get('max_voltage', 10) * 10.0
        release_condition = params.get('release_condition', '0')
        release_behavior = params.get('release_behavior', '0')

        if release_condition != '0' and release_condition == 0:
            if release_behavior == '0' or release_behavior == 'stay':
                last_value = self.get_last_value()
                if last_value is None:
                    value = 10
                else:
                    value = last_value
            elif release_behavior == 'off':
                value = 0
            elif release_behavior == 'on':
                value = 100
            elif release_behavior == 'min_ana':
                value = int(min_voltage)
            elif release_behavior == 'max_ana':
                value = int(max_voltage)

            status = memcache.get('%s%sstatus' % (type(self).__name__, self.pk)) or {'control': self.name, 'description': self.description}
            hc = HeatingCurve.objects.get(pk=params['heatingcurve'])
            setpoint = min(hc.get_value(), float(params['max_temp']))
            status['setpoint'] = setpoint
            status['position'] = value
            status['error'] = False
            status['msg'] = u"Freigabebedingung nicht erfüllt"
            params = self.get_parameters()
            outflow = params['outflow']
            outflow = Sensor.objects.get(name=outflow).get_value()
            status['outflow'] = outflow
            status['atsvalue'] = hc.get_temperature()
            if params.get('vtrtype', '3') == '3':
                try:
                    inflow_hot = params['inflow_hot']
                    inflow_hot = Sensor.objects.get(name=inflow_hot).get_value()
                except Sensor.DoesNotExist:
                    inflow_hot = None
                try:
                    inflow_cold = params['inflow_cold']
                    inflow_cold = Sensor.objects.get(name=inflow_cold).get_value()
                except Sensor.DoesNotExist:
                    inflow_cold = None
                status['inflow_hot'] = inflow_hot
                status['inflow_cold'] = inflow_cold
            memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
        
        else:
            if vtrtype == '4':
                value = self._010vmapping()            
            elif controller == 'normal':
                if vtrtype == '2' or (vtrtype == '3' and outflow is None):  # zweite bedingung ist legacy
                    value = self._simpledirectmc()
                elif vtrtype == '3':
                    value = self._simpleproportionalmc()
                elif vtrtype == '1':
                    value = self._simpleproportionalmc()
                else:
                    value = 0
            elif controller == 'pi':
                value = self._pid()
            elif controller == 'pid':
                value = self._pid()
            else:
                value = 0

            if min_voltage > value >= 0:
                value = int(min_voltage)
                status = memcache.get('%s%sstatus'% (type(self).__name__, self.pk))
                status['position'] = value
                memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
            elif value > max_voltage:
                value = int(max_voltage)
                status = memcache.get('%s%sstatus'% (type(self).__name__, self.pk))
                status['position'] = value
                memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
            else:
                pass

        status = memcache.get('%s%sstatus'% (type(self).__name__, self.pk))
        if status is not None:
            logging.warning("###status###mr,%s,%s,%s,%s,%s,%s,%s" % (status.get('control'), status.get('inflow_cold'), status.get('inflow_hot'), status.get('outflow'), status.get('setpoint'), status.get('position'), calendar.timegm(datetime.utcnow().timetuple()) * 1000))

        return value

    def _010vmapping(self):
        ret = 0
        err = False
        status = {'control': self.name, 'description': self.description}

        try:
            last_value = self.get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "Kein letzter Wert vorhanden, setze Stellung auf 10%."
                ret = 10

            else:
                params = self.get_parameters()
                hc = HeatingCurve.objects.get(pk=params['heatingcurve'])
                setpoint = min(hc.get_value(), float(params['max_temp']))
                
                outflow = None
                if 'outflow' in params and params['outflow'] != '0':
                    outflow = params['outflow']
                    outflow = Sensor.objects.get(name=outflow).get_value()

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                status['atsvalue'] = hc.get_temperature()

                if outflow is None:
                    err = True
                    status['msg'] = 'Es fehlen Sensorwerte.'

                else:
                    if outflow and outflow > params['max_temp']:
                        ret = 0
                        status['msg'] = u"Vorlauftemperatur über zulässiger Höchsttemperatur, fahre auf 0%."

                    else:
                        min_voltage = params['min_voltage'] * 10.0
                        max_voltage = params['max_voltage'] * 10.0
                        min_voltage_temp = float(params['min_voltage_temp'])
                        max_voltage_temp = float(params['max_voltage_temp'])

                        if setpoint > max_voltage_temp:
                            ret = max_voltage
                        elif setpoint < min_voltage_temp:
                            ret = min_voltage
                        else:
                            ret = ((max_voltage_temp - setpoint) / (max_voltage_temp - min_voltage_temp))
                            ret = max_voltage - (max_voltage - min_voltage) * ret
        
        except Exception as e:
            logging.exception("mixercontrol._010v")
            err = True
            status['msg'] = "Unbekannter Fehler. Bitte kontaktieren Sie den Controme Support."
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
        return ret

    def _pid(self):

        ret = 0
        err = False
        status = {'control': self.name, 'description': self.description}

        try:
            last_value = self.get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "Kein letzter Wert vorhanden, setze Stellung auf 10%."
                ret = 10

            else:
                params = self.get_parameters()
                hc = HeatingCurve.objects.get(pk=params['heatingcurve'])
                setpoint = min(hc.get_value(), float(params['max_temp']))
                outflow = params['outflow']
                outflow = Sensor.objects.get(name=outflow).get_value()
                
                if params.get('vtrtype', '3') == '3':
                    inflow_hot = params['inflow_hot']
                    inflow_hot = Sensor.objects.get(name=inflow_hot).get_value()
                    inflow_cold = params['inflow_cold']
                    inflow_cold = Sensor.objects.get(name=inflow_cold).get_value()
                    status['inflow_hot'] = inflow_hot
                    status['inflow_cold'] = inflow_cold

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                status['atsvalue'] = hc.get_temperature()

                if outflow is None:
                    err = True
                    status['msg'] = "Es fehlen Sensorenwerte."

                else:
                    if outflow > params['max_temp']:
                        ret = 0
                        status['msg'] = u"Vorlauftemperatur über zulässiger Höchsttemperatur, fahre auf 0%."

                    else:
                        controller = params['controller']
                        kp = params['kp']
                        ki = params['ki']
                        kd = params['kd']
                        windupguard = 100 * 1/ki

                        error = setpoint - outflow
                        last_status = memcache.get('%s%sstatus' % (type(self).__name__, self.pk)) or {'outflow': outflow, 'setpoint': setpoint, 'ts': time.time(), '_error': error, 'pi': 0}

                        if math.fabs(error) < params.get('hysteresis', 0) and last_status.get('position') is not None:
                            ret = last_status.get('position')
                            last_status.update(status)
                            memcache.set('%s%sstatus' % (type(self).__name__, self.pk), last_status)
                            return ret  # hier return, damit nicht unten %s%sstatus aktualisiert wird

                        else:
                            ct = time.time()
                            status['ts'] = ct
                            status['_error'] = error
                            dt = (ct - (last_status.get('ts') or time.time())) / self.scan_interval

                            pi = last_status.get('pi') or 0
                            if dt >= 1:
                                dt = min(dt, max(3, float(self.runtime)/float(self.scan_interval)))  # rt/si maximal ~15
                                pp = kp * error

                                pi += error*dt
                                if pi < 0:
                                    pi = 0
                                elif pi > windupguard:
                                    pi = windupguard

                                pd = 0
                                if controller == 'pid':
                                    error /= dt  # erst hier normalisieren wegen berechnung von pi
                                    status['_error'] = error  # wert neu berechnet, also in status neu setzen
                                    de = error - (last_status.get('_error') or 0)
                                    pd = de  # /dt  # wird oben schon dividiert

                                ret = pp + ki*pi + kd*pd
                                ret = max(0, min(100, ret))

                            else:
                                ret = last_status.get('position')
                                if ret is None:
                                    logging.warning("dt <= 1, position from ls is None, setting to 0")
                                    ret = 0

                            status['pi'] = pi

        except Exception as e:
            logging.exception("mixercontrol._pid")
            err = True
            status['msg'] = 'Unbekannter Fehler. Bitte kontaktieren Sie den Controme Support.'
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
        return ret

    def _simpleproportionalmc(self):

        ret = 0
        err = False
        status = {'control': self.name, 'description': self.description}

        try:
            last_value = self.get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "Kein letzter Wert vorhanden, setze Stellung auf 10%."
                ret = 10

            else:
                params = self.get_parameters()
                hc = HeatingCurve.objects.get(pk=params['heatingcurve'])
                setpoint = min(hc.get_value(), float(params['max_temp']))
                outflow = params['outflow']
                outflow = Sensor.objects.get(name=outflow).get_value()

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                status['atsvalue'] = hc.get_temperature()

                if params.get('vtrtype', '3') == '3':
                    try:
                        inflow_hot = params['inflow_hot']
                        inflow_hot = Sensor.objects.get(name=inflow_hot).get_value()
                    except Sensor.DoesNotExist:
                        inflow_hot = None
                    try:
                        inflow_cold = params['inflow_cold']
                        inflow_cold = Sensor.objects.get(name=inflow_cold).get_value()
                    except Sensor.DoesNotExist:
                        inflow_cold = None
                    status['inflow_hot'] = inflow_hot
                    status['inflow_cold'] = inflow_cold

                if outflow is None:
                    err = True
                    status['msg'] = "Es fehlen Sensorwerte."

                else:
                    if outflow > params['max_temp']:
                        ret = 0
                        status['msg'] = u"Vorlauftemperatur über zulässiger Höchsttemperatur, fahre auf 0%."

                    else:
                        gain = params.get('gain', 1.0)
                        drive_increment = params.get('drive_increment', 0.5)
                        max_increment = params.get('max_increment', 10)
                        hysteresis = params.get('hysteresis', 1.0)
                        last_status = memcache.get('%s%sstatus' % (type(self).__name__, self.pk)) or {'outflow': outflow, 'setpoint': setpoint}

                        if outflow > (setpoint + hysteresis) or outflow < (setpoint - hysteresis):
                            error = setpoint - outflow  # negative when outflow > setpoint

                            d = error * gain
                            if math.fabs(d) > max_increment:
                                d = math.copysign(max_increment, d)  # absolute value of m_i with the sign of d
                            elif math.fabs(d) < drive_increment:
                                d = 0

                            delta_out = 0
                            if (last_status.get('setpoint', setpoint) - hysteresis) <= setpoint <= (last_status.get('setpoint', setpoint) + hysteresis):
                                delta_out = (outflow - last_status['outflow']) * gain

                            ret = last_value + d - delta_out

                        else:
                            ret = last_value

                        ret = max(0, min(100, ret))

        except Exception as e:
            logging.exception("mixercontrol._simpleproportionalmc")
            err = True
            status['msg'] = 'Unbekannter Fehler. Bitte kontaktieren Sie den Controme Support.'
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
        return ret

    def _simpledirectmc(self):

        ret = 0
        err = False
        status = {'control': self.name, 'description': self.description}

        try:
            params = self.get_parameters()
            hc = HeatingCurve.objects.get(pk=params['heatingcurve'])
            setpoint = min(hc.get_value(), float(params['max_temp']))
            # outflow = params['outflow']
            # outflow = Sensor.objects.get(name=outflow).get_value()
            inflow_hot = params['inflow_hot']
            inflow_hot = Sensor.objects.get(name=inflow_hot).get_value()
            inflow_cold = params['inflow_cold']
            inflow_cold = Sensor.objects.get(name=inflow_cold).get_value()

            status['setpoint'] = setpoint
            # status['outflow'] = outflow  # ist None in self.get_output_state()
            status['inflow_hot'] = inflow_hot
            status['inflow_cold'] = inflow_cold
            status['atsvalue'] = hc.get_temperature()

            if inflow_hot is None or inflow_cold is None:
                err = True
                status['msg'] = "Es fehlen Sensorwerte."

            else:
                if inflow_hot < setpoint and inflow_cold < setpoint:
                    if inflow_hot >= inflow_cold:
                        ret = 100
                    else:
                        ret = 0

                elif inflow_hot > setpoint > inflow_cold:
                    diff = inflow_hot - inflow_cold
                    ret = 1-(inflow_hot-setpoint)/diff
                    ret *= 100

                elif inflow_hot < setpoint < inflow_cold:
                    diff = inflow_cold - inflow_hot
                    ret = (inflow_cold-setpoint)/diff
                    ret *= 100

                else:  # beide groesser
                    if inflow_hot >= inflow_cold:
                        ret = 0
                    else:
                        ret = 100

        except Exception as e:
            logging.exception("mixercontrol._simpledirectmc")
            err = True
            status['msg'] = 'Unbekannter Fehler. Bitte kontaktieren Sie den Controme Support.'
            ret = 0

        status['position'] = ret
        status['error'] = err
        memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)
        return ret

    class Meta:
        app_label = 'brgwconfiguration'


class ControlCycle(CachingMixin, ControlEntity):

    objects = CachingManager()

    @cache_state
    def get_output_state(self):
        # kann ja potentiell mehrere outputs haben, die unterschiedlich
        # geschalten werden. sollte also ein dict zurueckgeben
        if self.control_algorithm == "singlecycledifferentialcontrol":
            try:
                value = self._singlecycledifferentialc()
            except:
                value = 0
                logging.exception("cc._singlecycledifferentialc()")
        else:
            value = 0

        value = max(0, min(100, value))

        status = memcache.get('%s%sstatus'% (type(self).__name__, self.pk))
        if status is not None:
            logging.warning("###status###dr,%s,%s,%s,%s,%s,%s" % (status.get('control'), status.get('s1'), status.get('s2'), status.get('s3'), status.get('position'), calendar.timegm(datetime.utcnow().timetuple()) * 1000))

        return value

    def _singlecycledifferentialc(self):

        status = {'control': self.name, 'description': self.description}

        params = self.get_parameters()
        s1s = params['s1']
        if s1s is not None:
            s1 = Sensor.objects.get(name=s1s).get_value()
        else:
            s1 = None
        s2 = params['s2']
        s2 = Sensor.objects.get(name=s2).get_value()
        s3s = params['s3']
        if s3s is not None:
            s3 = Sensor.objects.get(name=s3s).get_value()
        else:
            s3 = None

        # todo: die s1 werte an tatsaechliche pt1000 fehlerwerte anpassen
        if (s1s is not None and s1 is None) or (s1 == -51.0 or s1 == 151.0 or (params['s1'] != 'pt1000' and s1 == 85.0)) or s2 is None or (s3s is not None and s3 is None):
            status['error'] = True
            ret = 0

        else:
            diff_s2 = float(params['diff_s2'])
            min_s1 = float(params['min_s1'])
            max_s1 = float(params.get('max_s1', 90))
            max_s2 = float(params['max_s2'])
            try:
                max_s3 = float(params['max_s3'])
            except (ValueError, TypeError):
                logging.exception("max_s3 exception, setting to 85")
                max_s3 = 85
            try:
                einvzg = int(params.get('einvzg', 0))
            except (ValueError, TypeError):
                logging.exception("einvzg exception, setting to 0")
                einvzg = 0
            try:
                ausvzg = int(params.get('ausvzg', 0))
            except (ValueError, TypeError):
                logging.exception("ausvzg exception, setting to 0")
                ausvzg = 0
            hysteresis = float(params['hysteresis'])

            ret = s2 < max_s2
            if s1 is not None:
                ret &= (s1 > (s2 + diff_s2)) & (max_s1 > s1 > min_s1)
            if s3 is not None:
                ret &= (s3 < max_s3)
            ret = 100 if ret else 0  # sonst ist die berechnung ein boolean

            invertout = params.get('invertout', False)

            ls = memcache.get("%s%sstatus" % (type(self).__name__, self.pk))
            do_delay = False
            if ls and ls.get('position') is not None:

                last_position = ls['position'] if not invertout else 100 if ls['position'] == 0 else 0

                if s1 is None and s3 is None:
                    if ret == 100 and last_position == 0 and s2 < (max_s2 - hysteresis):
                        do_delay = True
                    elif ret == 0 and last_position == 100 and s2 > (max_s2 + hysteresis):
                        do_delay = True
                    else:
                        ret = last_position
                        status['applied_hysteresis'] = True

                elif s3 is None:
                    if max_s1 > s1 > min_s1 and s2 < max_s2:
                        if ret == 100 and last_position == 0 and s1 > (s2 + diff_s2 + hysteresis):
                            do_delay = True
                        elif ret == 0 and last_position == 100 and s1 < (s2 + diff_s2 - hysteresis):
                            do_delay = True
                        else:
                            ret = last_position
                            status['applied_hysteresis'] = True

                else:
                    if max_s1 > s1 > min_s1 and s2 < max_s2 and s3 < max_s3:
                        if ret == 100 and last_position == 0 and s1 > (s2 + diff_s2 + hysteresis):
                            do_delay = True
                        elif ret == 0 and last_position == 100 and s1 < (s2 + diff_s2 - hysteresis):
                            do_delay = True
                        else:
                            ret = last_position
                            status['applied_hysteresis'] = True

            else:
                if (s1 is None and s3 is None) or (s3 is None and max_s1 > s1 > min_s1 and s2 < max_s2) or (max_s1 > s1 > min_s1 and s2 < max_s2 and s3 < max_s3):
                    # zwar kein vorheriger wert, aber auch keine sicherheitsbedingung aktiv:
                    do_delay = True

            if invertout:
                ret = 100 if ret == 0 else 0

            if do_delay and (einvzg or ausvzg):
                last = memcache.get("cclast_%s" % self.id)
                if ret:
                    # ret = 100
                    if last:

                        if last[1] > 0:
                            pass

                        else:
                            if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                                last = (datetime.now(), ret)
                                memcache.set("cclast_%s" % self.id, last)

                        if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                            pass

                        else:
                            ret = 0
                            status['applied_ausvzg'] = True

                    else:
                        memcache.set("cclast_%s" % self.id, (datetime.now(), ret))
                        ret = 0
                        status['applied_ausvzg'] = True

                else:
                    # ret = 0
                    if last:

                        if last[1] == 0:
                            pass

                        else:
                            if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                                last = (datetime.now(), 0)
                                memcache.set("cclast_%s" % self.id, last)

                        if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                            pass

                        else:
                            ret = 100
                            status['applied_einvzg'] = True

                    else:
                        memcache.set("cclast_%s" % self.id, (datetime.now(), 0))
                        ret = 100
                        status['applied_einvzg'] = True

        status['s1'] = s1
        status['s2'] = s2
        status['s3'] = s3
        status['position'] = ret  # 0-100, not runtime-adjusted!
        memcache.set('%s%sstatus' % (type(self).__name__, self.pk), status)

        return ret

    class Meta:
        app_label = 'brgwconfiguration'
