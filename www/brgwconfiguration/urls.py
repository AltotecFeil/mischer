from django.conf.urls import patterns, url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^sensors/$', views.sensors, name="sensors"),
    url(r'^inputs/$', views.inputs, name="inputs"),
    url(r'^settings/$', views.settings, name="settings"),
    url(r'^dr/$', views.drs, name="dr"),
    url(r'^dr/(?P<dregid>(\d+))/$', views.drs, name="dr"),
    url(r'^vtr/$', views.mrs, name="vtr"),
    url(r'^vtr/(?P<mid>(\d+))/$', views.mrs, name="vtr"),
    url(r'^otherouts/$', views.otherouts, name="otherouts"),
]
