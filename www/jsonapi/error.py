from django.http import HttpResponse
import requests
import sys, traceback
from fabric.api import local, lcd
from datetime import datetime
import socket, fcntl, struct
import logging


def get_mac():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', 'eth0'))
    mac = '-'.join(['%02x' % ord(char) for char in info[18:24]])
    return mac.lower()


def error500(request):

    try:
        typ, value, trace = sys.exc_info()
        branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
        res = local("git log -1 --date=raw", capture=True)
        luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
        lu = datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
        ret = {
            'get': str(request.GET),
            'post': str(request.POST),
            'path': request.path,
            'exc_type': typ.__name__,
            'exc_msg': typ.__doc__,
            'exc_value': str(value).translate(None, "'").translate(None, '"').translate(None, "!"),
            'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)], 
            'branch': branch,
            'rev': lu
        }
        with lcd("/home/pi/mischer"):
            enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
        # err = enc
        macaddr = get_mac()
        requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % enc)
    except Exception as e:
        logging.exception("exception sending exception")
        pass

    return HttpResponse('Ein unvorhergesehenes Problem ist aufgetreten.')


def error404(request):

    return HttpResponse("Seite konnte nicht gefunden werden.")
