from django.conf.urls import patterns, url
from . import views

urlpatterns = [
    url(r'^v(?P<jsonver>\d+)/(?P<data>[A-Za-z]+)/$', views.jsonapi),
    url(r'^v(?P<jsonver>\d+)/(?P<data>[A-Za-z]+)/(?P<entityid>\d+)/$', views.jsonapi),

    url(r'^(?P<sensorssn>([0-9A-Fa-f]{2}[_]){7}([0-9A-Fa-f]{2}))/(?P<ist>([-]*\d+\.\d{2}))/$', views.set_sensor),
    url(r'^master/$', views.set_master_ip),
]
