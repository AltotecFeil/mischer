from brgwconfiguration.models import BRGWInput, Sensor, Mixer, HeatingCurve, BRGWOutput, RPi, BoilerRoomGateway, BRGWPin, ControlCycle
from django.http import HttpResponse
import logging
import json
from django.core.cache import cache as memcache
import re
import requests
from requests.exceptions import ReadTimeout
import fcntl, socket, struct
from datetime import datetime, timedelta
from fabric.api import local


def get_mac():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', 'eth0'))
    mac = '-'.join(['%02x' % ord(char) for char in info[18:24]])
    return mac.lower()
    # return "b8-27-eb-f2-02-3a"


def jsonapi(request, jsonver, data, entityid=None):
    return globals()[data](request, entityid)


def miniservers(request, name=None):

    if request.method == "GET" and request.META['REMOTE_ADDR'] in {'localhost', '127.0.0.1'}:
        ret = []
        for rpi in RPi.objects.all():
            ret.append({
                rpi.name: rpi.ip
            })
        return HttpResponse(json.dumps(ret))

    elif request.method == "POST" and request.META['REMOTE_ADDR'] in {'localhost', '127.0.0.1'}:

        data = request.body
        try:
            data = json.loads(data)
        except (TypeError, ValueError):
            return HttpResponse(status=400)

        for mac, ip in data.items():
            try:
                rpi = RPi.objects.get(name=mac.lower())
                if rpi.ip != ip:
                    rpi.ip = ip
                    rpi.save()
            except RPi.DoesNotExist:
                # if not len(RPi.objects.all()):
                rpi = RPi(name=mac.lower(), ip=ip, description='')
                rpi.save()

        return HttpResponse()


def sensors(request, ssn=None):

    if request is None or request.method == "GET":
        ret = []
        if ssn:
            _sensors = Sensor.objects.filter(name=ssn)
        else:
            _sensors = Sensor.objects.all().order_by("name")
        for sensor in _sensors:
            ret.append({
                sensor.name: {
                    'value': sensor.get_value(),
                    'linie': sensor.linie
                }
            })
        return HttpResponse(json.dumps(ret))

    elif request.method == "POST" and request.META['REMOTE_ADDR'] in {'localhost', '127.0.0.1'}:

        data = request.body
        try:
            data = json.loads(data)
        except (TypeError, ValueError):
            return HttpResponse(status=400)
        macaddr = get_mac()

        if 'pt1000' in data:
            try:
                pt1000 = Sensor.objects.get(name="pt1000")
            except Sensor.DoesNotExist:
                try:
                    brgw = BoilerRoomGateway.objects.get(name=macaddr)
                except BoilerRoomGateway.DoesNotExist:
                    macaddr = get_mac()
                    brgw = BoilerRoomGateway(name=macaddr)
                    brgw.save()
                pt1000 = Sensor(name='pt1000', brgw=brgw)
                pt1000.save()
            if data['pt1000']['value'] is not None:
                pt1000.set_value(_calc_pt1000(pt1000, data['pt1000']))
                data['pt1000']['value'] = pt1000.get_value()
            else:
                pt1000.set_value(None)

        for ssn, values in data.items():
            if ssn == 'pt1000':
                continue
            try:
                sensor = Sensor.objects.get(name='_'.join(re.findall('..', ssn)).lower())
            except Sensor.DoesNotExist:
                del data[ssn]
                continue

            try:
                if float(values['value']) != 85.0:
                    sensor.set_value(float(values['value']))
                if sensor.linie != values['linie']:
                    sensor.linie = values['linie']
                    sensor.save()
            except (TypeError, ValueError, KeyError):
                continue

        try:  # add universal inputs to sensors
            ui1 = BRGWInput.objects.get(name="ui1")
            data[u'ui10'] = {u'linie': -1, u'value': ui1.get_value()}
        except BRGWInput.DoesNotExist:
            pass
        try:
            ui2 = BRGWInput.objects.get(name="ui2")
            data['ui20'] = {u'linie': -1, u'value': ui2.get_value()}
        except BRGWInput.DoesNotExist:
            pass

        sensoren = [{'_'.join(re.findall('..', ssn)).lower(): values} for ssn, values in data.items()]

        for rpi in RPi.objects.all():
            try:
                r = requests.post("http://%s/set/%s/all/" % (rpi.ip, macaddr), json=sensoren, timeout=10)
            except:
                pass

        lu = memcache.get("laststatusupdateoutputcontrol")
        if lu is None:
            ts = datetime.utcnow() - timedelta(seconds=500)
            memcache.set("laststatusupdateoutputcontrol", ts)
        else:
            if lu > (datetime.utcnow() + timedelta(seconds=630)):
                local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart mischersteuerung")

        return HttpResponse()

    else:
        return HttpResponse(status=405)


def inputs(request, input=None):
    if request is None or request.method == "GET":
        ret = []
        try:
            _inputs = BRGWInput.objects.all().order_by("name")
        except:
            return HttpResponse()
        for _input in _inputs:
            ret.append({
                _input.name: {
                    'value': _input.get_value()
                }
            })
        return HttpResponse(json.dumps(ret))

    elif request.method == "POST" and request.META['REMOTE_ADDR'] in {'localhost', '127.0.0.1'}:
        data = request.body
        try:
            data = json.loads(data)
        except (TypeError, ValueError):
            return HttpResponse(status=400)
        macaddr = get_mac()

        for uistr in {'ui1', 'ui2'}:
            if uistr in data:
                try:
                    ui = BRGWInput.objects.get(name=uistr)
                except BRGWInput.DoesNotExist:
                    try:
                        brgw = BoilerRoomGateway.objects.get(name=macaddr)
                    except BoilerRoomGateway.DoesNotExist:
                        macaddr = get_mac()
                        brgw = BoilerRoomGateway(name=macaddr)
                        brgw.save()
                    ui = BRGWInput(name=uistr, brgw=brgw)
                    ui.save()
                if data[uistr]['value'] is not None:
                    ui.set_value(data[uistr]['value'])
                else:
                    ui.set_value(None)

        return HttpResponse()

    else:
        return HttpResponse(status=405)


def _calc_pt1000(pt1000, mv):
    if mv is None:
        return None
    params = pt1000.brgw.get_parameters()
    rtab = [-1, 5281, 5468, 5667, 5839, 6000, 6156, 6312, 6477, 6635, 6780, 6927, 7069, 7209, 7343, 7478, 7608, 7732, 7857, 7973, 8090, 8195, 8300, -1, -1]
    ttab = [ 0,  -50,  -40,  -30,  -20,  -10,    0,   10,   20,   30,   40,   50,   60,   70,   80,   90,  100,  110,  120,  130,  140,  150,  160, 170, 0]
    if 'pt1000' in params:
        # shift tab
        pass
    val = mv['value']*4

    idx = 0
    for i, r in enumerate(rtab[1:]):
        if r > val:
            idx = i+1
            break
    v1 = rtab[idx-1]
    v2 = rtab[idx]

    if v1 > 0 and v2 > 0:
        diff = v2 - v1
        t = ttab[idx]
        return t - (float(v2) - val)/(diff/10.0)
    else:
        if -1 < val < 5281:
            return -51.0
        # return 151.0
        return None


def mixers(request, mixer_id=None):

    if request.method == "GET":
        macaddr = get_mac()

        lu = memcache.get("last_mixer_config_update")
        min_mixer_interval = 180
        for mixer in Mixer.objects.all():
            if mixer.scan_interval < min_mixer_interval:
                min_mixer_interval = mixer.scan_interval
        if lu is None or lu + timedelta(seconds=min_mixer_interval) < datetime.utcnow():
            memcache.set("last_mixer_config_update", datetime.utcnow())
            for rpi in list(RPi.objects.all()):
                try:
                    resp = requests.get("http://%s/get/mixer/%s/config/" % (rpi.ip, macaddr), timeout=10)
                except (requests.ConnectionError, ReadTimeout) as e:
                    logging.exception("connerror/timeout")
                    try:
                        r = requests.post("http://%s/set/mixer/%s/status/" % (rpi.ip, macaddr), json={"error": "Fehler beim VTR-Konfigurationsabruf vom Miniserver (%s)." % str(e)}, timeout=10)
                    except Exception as e:
                        logging.exception("exception retrieving mixer config")
                    continue
                try:
                    data = json.loads(resp.content)
                except ValueError:
                    logging.exception("ValueError")
                    continue
                # todo die config nur schreiben, wenn wir auch was zurueckbekommen haben ... weil sonst wird in _write_mixer_config geloescht
                _write_mixer_config(data, rpi.name)

        ret = []
        if mixer_id:
            try:
                mixer_id = long(mixer_id)
                _mixers = Mixer.objects.filter(pk=mixer_id)
            except (ValueError, TypeError):
                _mixers = []
        else:
            _mixers = Mixer.objects.all()
        for mixer in _mixers:
            params = mixer.get_parameters()
            ret.append({
                mixer.id: {
                    'output_id': mixer.brgwoutput_id,  # could be more than one at some point
                    'runtime': mixer.runtime,
                    'position': mixer.get_output_state(),
                    'scan_interval': mixer.scan_interval,
                    'max_temp': params['max_temp'],
                    'max_temp_sensor': params['outflow'],
                    'calibration_interval': params.get('calibration_interval', 7),
                    'master': mixer.master,
                    'name': mixer.name,
                    'description': mixer.description,
                    'vtrtype': params.get('vtrtype', 3),
                }
            })

        logging.warning(ret)
        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(status=405)


def _write_mixer_config(data, master_mac):

    for mixer in Mixer.objects.filter(master=master_mac):
        if str(mixer.name) not in data.keys():
            mixer.delete()

    for mid, values in data.items():

        if mid == 'sensors' or mid == 'sensor_offsets' or not mid.isdigit():
            continue

        try:
            brgw = BoilerRoomGateway.objects.get(name=get_mac())
        except BoilerRoomGateway.DoesNotExist:
            macaddr = get_mac()
            brgw = BoilerRoomGateway(name=macaddr)
            brgw.save()

        try:
            mixer = Mixer.objects.get(name=int(mid))
            mparams = mixer.get_parameters()
            brgwo = mixer.brgwoutput
            curve = HeatingCurve.objects.get(pk=mparams['heatingcurve'])
        except Mixer.DoesNotExist:
            curve = HeatingCurve(slope=0.0, bend=0.0, offset=0.0)
            curve.save()
            brgwo = BRGWOutput(brgw=brgw)
            brgwo.save()
            mixer = Mixer(name=int(mid), brgwoutput=brgwo, master=master_mac.lower())
            mparams = {'heatingcurve': curve.pk}

        curve.bend = values['bend']
        curve.slope = values['slope']
        curve.offset = float(values['offset']) + values.get('module_offset', 0)
        curve.save()

        pins = BRGWPin.objects.filter(brgwoutput_id=mixer.brgwoutput_id)
        for pin in pins:
            pin.dependent_pin = 0
            pin.save()
        for pin in pins:
            pin.delete()

        ana = values.get('outputs', dict()).get('ana', list())
        for a in ana:
            pin = BRGWPin(name="", dependent_pin=0, number=a, pin_type="ana", brgwoutput=brgwo, parameters="{}")
            pin.save()
        don = values.get('outputs', dict()).get('don', list())
        doff = values.get('outputs', dict()).get('doff', list())
        if len(don) and len(doff):
            pinon = BRGWPin(name="", dependent_pin=0, number=don[0], pin_type="dig_on", brgwoutput=brgwo, parameters="{}")
            pinon.save()
            pinoff = BRGWPin(name="", dependent_pin=pinon.id, number=doff[0], pin_type="dig_off", brgwoutput=brgwo, parameters="{}")
            pinoff.save()
            pinon.dependent_pin = pinoff.id
            pinon.save()

        mparams['hysteresis'] = float(values['hysteresis'] or 0.0)
        mparams['drive_increment'] = float(values['drive_increment'])
        mparams['max_increment'] = float(values['max_increment'])
        mparams['gain'] = float(values['gain'] or 0.0)
        mparams['max_temp'] = int(values['max_temp'] or 50)
        mparams['outflow'] = values['outs']
        mparams['inflow_hot'] = values['inws']
        mparams['inflow_cold'] = values['incs']
        mparams['vtrtype'] = values.get('vtrtype', '3')
        mparams['heatingcurve'] = curve.pk
        mparams['controller'] = values.get('controller', 'normal')
        mparams['kp'] = float(values.get('kp', 1))
        mparams['ki'] = float(values.get('ki', 0))
        mparams['kd'] = float(values.get('kd', 0))
        try:
            mparams['min_voltage'] = max(0, float(values.get('min_voltage', 0)))
        except:
            mparams['min_voltage'] = 0
        try:
            mparams['max_voltage'] = min(10, float(values.get('max_voltage', 10)))
        except:
            mparams['max_voltage'] = 10
        if values.get('vtrtype', '3') == '4':
            try:
                mparams['min_voltage_temp'] = float(values.get('min_voltage_temp'))
                mparams['max_voltage_temp'] = float(values.get('max_voltage_temp'))
            except:
                pass
        mparams['release_condition'] = values.get('release_condition', '0')
        mparams['release_behavior'] = values.get('release_behavior', '0')
        try:
            mparams['calibration_interval'] = max(0.1, float(values.get('calibration_interval', "7").replace(',', '.')))
        except:
            mparams['calibration_interval'] = 7
        mixer.control_algorithm = values['control']
        mixer.scan_interval = int(values['scan_interval'])
        mixer.description = values.get('name', '')
        mixer.runtime = int(values['runtime'])
        mixer.set_parameters(mparams)
        mixer.save()

        for sid in ['inws', 'incs', 'outs']:
            if values[sid] is None:
                continue
            try:
                s = Sensor.objects.get(name=values[sid])
            except Sensor.DoesNotExist:
                s = Sensor(name=values[sid], brgw=brgw)
                s.save()
            except Sensor.MultipleObjectsReturned:
                for s in Sensor.objects.filter(name=values[sid])[1:]:
                    s.delete()
                s = Sensor.objects.get(name=values[sid])

        if values['ats'] is not None:
            if 'pt1000' in values['ats']:
                mac = values['ats'].split('#')[0]
                if mac == get_mac():
                    try:
                        s = Sensor.objects.get(name="pt1000")
                    except Sensor.DoesNotExist:
                        s = None
                    curve.outside_sensor = s
                    curve.save()
            else:
                try:
                    s = Sensor.objects.get(name=values['ats'])
                except Sensor.DoesNotExist:
                    s = Sensor(name=values['ats'], brgw=brgw)
                    s.save()
                curve.outside_sensor = s
                curve.save()
        elif 'atsvalue' in values:
            try:
                curve.outside_sensor = None
                curve.save()
                val = float(values['atsvalue'])
                memcache.set("atsvalue", val)
            except (ValueError, TypeError):
                pass

    macaddr = get_mac()
    try:
        brgw = BoilerRoomGateway.objects.get(name=macaddr)
    except BoilerRoomGateway.DoesNotExist:
        brgw = BoilerRoomGateway(name=macaddr)
        brgw.save()
    cnt_sensors = {s.name for s in Sensor.objects.filter(brgw=brgw)}
    try:
        cnt_sensors.remove('pt1000')
    except:
        pass
    if 'sensors' in data:
        for sensorssn in data.get('sensors', list()):
            try:
                cnt_sensors.remove(sensorssn)
            except:
                s = Sensor(name=sensorssn, brgw=brgw)
                s.save()
        for sensorssn in cnt_sensors:
            try:
                Sensor.objects.get(name=sensorssn).delete()
            except:
                continue
    if 'sensor_offsets' in data:
        for s2o in data.get('sensor_offsets', list()):
            ssn = s2o[0]
            if 'pt1000' in ssn:
                ssn = 'pt1000'
            offset = s2o[1]
            if offset is not None:
                memcache.set("offset_%s" % ssn, offset)


def differentialcontrols(request, dreg_id=None):

    if request.method == "GET":
        macaddr = get_mac()

        lu = memcache.get("last_cc_config_update")
        min_cc_interval = 180
        for cc in ControlCycle.objects.all():
            if cc.scan_interval < min_cc_interval:
                min_cc_interval = cc.scan_interval
        if lu is None or lu + timedelta(seconds=min_cc_interval) < datetime.utcnow():
            memcache.set("last_cc_config_update", datetime.utcnow())
            for rpi in RPi.objects.all():
                try:
                    resp = requests.get("http://%s/get/differentialcontrol/%s/config/" % (rpi.ip, macaddr), timeout=10)
                except (requests.ConnectionError, ReadTimeout) as e:
                    try:
                        r = requests.post("http://%s/set/differentialcontrol/%s/status/" % (rpi.ip, macaddr), json={"error": "Fehler beim Differenzregelungs-Konfigurationsabruf vom Miniserver (%s)." % str(e)}, timeout=10)
                    except Exception as e:
                        logging.exception("exception retrieving dreg config")
                    continue

                if resp.status_code == 405:
                    return HttpResponse(json.dumps({}))
                try:
                    data = json.loads(resp.content)
                except ValueError:
                    continue
                _write_differentialcontrol_config(data, rpi.name)

        ret = []
        if dreg_id:
            try:
                dreg_id = long(dreg_id)
                _dregs = ControlCycle.objects.filter(pk=dreg_id)
            except (ValueError, TypeError):
                _dregs = []
        else:
            _dregs = ControlCycle.objects.filter(control_algorithm="singlecycledifferentialcontrol")
        for dreg in _dregs:
            params = dreg.get_parameters()
            r = {dreg.id: {
                    'output_id': dreg.brgwoutput_id,
                    'position': dreg.get_output_state(),
                    'scan_interval': dreg.scan_interval,
                    'master': dreg.master,
                    'name': dreg.name,
                    'description': dreg.description,
                    's1': params['s1'],
                    's2': params['s2'],
                    's3': params['s3']
                }
            }
            r[dreg.id].update(params)
            ret.append(r)

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(status=405)


def _write_differentialcontrol_config(data, master_mac):

    for cc in ControlCycle.objects.filter(master=master_mac):
        if str(cc.name) not in data.keys():
            cc.delete()

    for dregid, values in data.items():
        if not dregid.isdigit():
            continue
        try:
            brgw = BoilerRoomGateway.objects.get(name=get_mac())
        except BoilerRoomGateway.DoesNotExist:
            macaddr = get_mac()
            brgw = BoilerRoomGateway(name=macaddr)
            brgw.save()

        try:
            cc = ControlCycle.objects.get(name=int(dregid), master=master_mac)
            ccparams = cc.get_parameters()
            brgwo = cc.brgwoutput
        except ControlCycle.DoesNotExist:
            brgwo = BRGWOutput(brgw=brgw)
            brgwo.save()
            cc = ControlCycle(name=int(dregid), brgwoutput=brgwo, master=master_mac)
            ccparams = {}

        pins = BRGWPin.objects.filter(brgwoutput_id=cc.brgwoutput_id)
        pins.delete()

        pinnos = values['outputs']
        for pinno in pinnos:
            pin = BRGWPin(name="", dependent_pin=0, number=pinno, pin_type='dig', brgwoutput=brgwo, parameters="{}")
            pin.save()

        cc.control_algorithm = values['control']
        cc.scan_interval = values.get('scan_interval', 60)
        cc.description = values.get('name', '')
        ccparams['s1'] = values['s1']
        ccparams['s2'] = values['s2']
        ccparams['s3'] = values['s3']
        ccparams['diff_s2'] = values['diff_s2']
        ccparams['min_s1'] = values['min_s1']
        ccparams['max_s1'] = values.get('max_s1', 90)
        ccparams['max_s2'] = values['max_s2']
        ccparams['max_s3'] = values['max_s3']
        ccparams['einvzg'] = values.get('einvzg', 0)
        ccparams['ausvzg'] = values.get('ausvzg', 0)
        ccparams['hysteresis'] = values['hysteresis']
        ccparams['invertout'] = values.get('invertout', False)
        cc.set_parameters(ccparams)
        cc.save()

        for sid in ['s1', 's2', 's3']:
            if not values[sid]: continue
            try:
                s = Sensor.objects.get(name=values[sid])
            except Sensor.DoesNotExist:
                s = Sensor(name=values[sid], brgw=brgw)
                s.save()
            if "%s_val" % sid in values:
                try:
                    s.set_value(float(values["%s_val" % sid]))
                except Exception as e:
                    logging.exception("error setting externally connected sensor")

    # cleanup. umstaendlich.
    brgwo_ids = set()
    for cc in ControlCycle.objects.all():
        brgwo_ids.add(cc.brgwoutput_id)
    for m in Mixer.objects.all():
        brgwo_ids.add(m.brgwoutput_id)
    for pin in BRGWPin.objects.all():
        if pin.brgwoutput_id not in brgwo_ids:
            pin.delete()
    for brgwo in BRGWOutput.objects.all():
        if brgwo.id not in brgwo_ids:
            brgwo.delete()


def otherouts(request, out_id=None):

    if request.method == "GET":

        macaddr = get_mac()
        ret = memcache.get("last_oouts_config") or {}
        lu = memcache.get("last_oouts_config_update")
        min_oouts_interval = 55
        if lu is None or lu + timedelta(seconds=min_oouts_interval) < datetime.utcnow():
            memcache.set("last_oouts_config_update", datetime.utcnow())

            for rpi in RPi.objects.all():

                mods = ['pl', 'fps', 'rlr', 'zpr', 'nr']
                for mod in mods:
                    try:
                        resp = requests.get("http://%s/get/%s/%s/config/" % (rpi.ip, mod, macaddr), timeout=10)
                    except (requests.ConnectionError, ReadTimeout) as e:
                        logging.exception("conne for mod %s" % mod)
                        try:
                            r = requests.post("http://%s/set/%s/%s/status/" % (rpi.ip, mod, macaddr), json={"error": "Fehler beim %s-Konfigurationsabruf vom Miniserver (%s)." % (mod.upper(), str(e))}, timeout=10)
                        except Exception as e:
                            logging.exception("exception retrieving %s config" % mod)
                    else:
                        try:
                            data = json.loads(resp.content)
                            ret.setdefault(mod, dict())
                            ret[mod].update(data)
                        except Exception as e:
                            logging.exception("exception retrieving %s from %s" % (mod, rpi.name))
                            pass

            memcache.set("last_oouts_config", ret)

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(status=405)


def outputs(request, output_id=None):

    if request.method == "GET":
        ret = []
        if output_id:
            try:
                output_id = long(output_id)
                _outputs = BRGWOutput.objects.filter(pk=output_id)
            except (ValueError, BRGWOutput.DoesNotExist):
                _outputs = []
        else:
            _outputs = BRGWOutput.objects.all()
        for output in _outputs:
            ret.append({
                output.id: {
                    'brgw': output.brgw_id,
                    'analogpin': [p.number for p in output.analogpins],
                    'digitalpin': [p.number for p in output.digitalpins],
                    'digital_on': [p.number for p in output.digital_on],
                    'digital_off': [p.number for p in output.digital_off],
                }
            })

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(status=405)


def heatingcurves(request, heatingcurve_id=None):

    if request.method == "GET":
        ret = []
        if heatingcurve_id:
            try:
                heatingcurve_id = long(heatingcurve_id)
                _curves = HeatingCurve.objects.filter(pk=heatingcurve_id)
            except (ValueError, TypeError):
                _curves = []
        else:
            _curves = HeatingCurve.objects.all()
        for curve in _curves:
            ret.append({
                curve.id: {
                    'bend': curve.bend,
                    'slope': curve.slope,
                    'offset': curve.offset,
                    'value': curve.get_value()
                }
            })
        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(status=405)


# todo das liefert jetzt nur den status aller cc und mixer gleichzeitig zurueck
def status(request, mixer_id=None):

    if request.method == "GET":
        ret = []
        _mixers = json.loads(mixers(request, None).content)
        for mixer in _mixers:
            mixer = Mixer.objects.get(pk=long(mixer.keys()[0]))
            last_lstts = memcache.get("%s%sls" % (type(mixer).__name__, mixer.pk))
            last_status = memcache.get("%s%sstatus" % (type(mixer).__name__, mixer.pk))

            if last_status:
                last_status = last_status
                last_status.update({'last_update': last_lstts})
            else:
                last_status = {}
            last_status['mixer_id'] = mixer.id
            last_status['output_id'] = mixer.brgwoutput_id if mixer.brgwoutput_id else None
            ret.append(last_status)

        _ccs = json.loads(differentialcontrols(request, None).content)
        for cc in _ccs:
            cc = ControlCycle.objects.get(pk=long(cc.keys()[0]))
            last_lstts = memcache.get("%s%sls" % (type(cc).__name__, cc.pk))
            last_status = memcache.get("%s%sstatus" % (type(cc).__name__, cc.pk))

            if last_status:
                last_status = last_status
                last_status.update({'last_update': last_lstts})
            else:
                last_status = {}
            last_status['cc_id'] = cc.id
            last_status['output_id'] = cc.brgwoutput_id if cc.brgwoutput_id else None
            ret.append(last_status)

        return HttpResponse(json.dumps(ret))

    elif request.method == "POST" and request.META['REMOTE_ADDR'] in {'127.0.0.1', 'localhost'}:

        mixerstatusdata = {}
        ccstatusdata = {}
        data = json.loads(request.body)
        memcache.set("laststatusupdateoutputcontrol", datetime.utcnow())
        for obj, ts in data.items():
            obj = obj.split('_')

            if obj[0] == 'mixer':
                mixer_id = int(obj[1])
                mixer = Mixer.objects.get(pk=mixer_id)
                mixerstatusdata[mixer.name] = {"last_drive": ts}
                mixerstatusdata[mixer.name].update({"last_update": memcache.get("%s%sls" % (type(mixer).__name__, mixer.pk))})
                last_status = memcache.get("%s%sstatus" % (type(mixer).__name__, mixer.pk))
                logging.warning("json mixer ls: %s" % str(last_status))

                if last_status is not None:
                    mixerstatusdata[mixer.name].update(last_status)

            elif obj[0] == 'cc':
                cc_id = int(obj[1])
                cc = ControlCycle.objects.get(pk=cc_id)
                ccstatusdata[cc.name] = {'last_drive': ts}
                ccstatusdata[cc.name].update({"last_update": memcache.get("%s%sls" % (type(cc).__name__, cc.pk))})
                last_status = memcache.get("%s%sstatus" % (type(cc).__name__, cc.pk))
                logging.warning("json cc ls: %s" % str(last_status))
                if last_status is not None:
                    ccstatusdata[cc.name].update(last_status)

        rpis = RPi.objects.all()
        macaddr = get_mac()
        for rpi in rpis:
            try:
                r = requests.post("http://%s/set/mixer/%s/status/" % (rpi.ip, macaddr), json=mixerstatusdata, timeout=10)
            except Exception as e:
                logging.exception("exception setting dreg status on master")
                try:
                    r = requests.post("http://%s/set/mixer/%s/status/" % (rpi.ip, macaddr), json={"error": "Fehler bei der VTR-Statusmeldung zwischen Miniserver und Heizraumgateway (%s)." % str(e)}, timeout=10)
                except Exception as e:
                    logging.exception("exception sending mixer exc to master")
            try:
                r = requests.post("http://%s/set/differentialcontrol/%s/status/" % (rpi.ip, macaddr), json=ccstatusdata, timeout=10)
            except Exception as e:
                logging.exception("exception setting dreg status on master")
                try:
                    r = requests.post("http://%s/set/differentialcontrol/%s/status/" % (rpi.ip, macaddr), json={"error": "Fehler bei der Differenzregelungs-Statusmeldung zwischen Miniserver und Heizraumgateway (%s)." % str(e)}, timeout=10)
                except Exception as e:
                    logging.exception("exception sending dreg exc to master")


        return HttpResponse()

    else:
        return HttpResponse(status=405)


def set_sensor(request, sensorssn, ist):

    if request.method == "GET":
        try:
            sensor = Sensor.objects.get(name=sensorssn.lower())
        except Sensor.DoesNotExist:
            return HttpResponse(status=400)

        try:
            sensor.set_value(float(ist))
        except (TypeError, ValueError):
            return HttpResponse(status=400)

        return HttpResponse()

    else:
        return HttpResponse(status=405)


def set_master_ip(request):

    if request.method == "GET":
        rpi = None
        if 'mac' in request.GET:
            m = re.match(r'([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2})', request.GET['mac'].lower())
            if not m:
                return HttpResponse("no valid mac supplied", status=405)
            try:
                rpi = RPi.objects.get(name=request.GET['mac'].lower())
                # this is only allowed once
                # return HttpResponse(status=405)
            except RPi.DoesNotExist:
                rpi = RPi(name=request.GET['mac'].lower(), description='')

        if 'ip' in request.GET and rpi is not None:
            rpi.ip = request.GET['ip']

        if rpi is not None:
            rpi.save()
            return HttpResponse("set ip address to %s for rpi %s" % (rpi.ip, rpi.name))
        else:
            return HttpResponse("no valid mac/ip supplied", status=405)

    else:
        return HttpResponse(status=405)
