from django.conf.urls import url, include
from jsonapi import error

handler500 = error.error500
handler404 = error.error404

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^json/', include('jsonapi.urls')),
    url(r'^', include('brgwconfiguration.urls')),
]
