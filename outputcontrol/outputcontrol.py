import requests
import json
import time
import zmq
import logging
from datetime import datetime, timedelta
from fabric.api import local
import memcache as mc

DEBUG = False 

memcache = mc.Client(['127.0.0.1:11211'])


class Mischersteuerung:

    def __init__(self):
        self.debug = DEBUG
        result = local("uname -a", capture=True)
        if 'contromeminiserver' in result:
            self.port = ":80"
        else:
            self.port = ":8000"
        self.context = zmq.Context()
        self.outsocket = self.context.socket(zmq.DEALER)
        self.outsocket.connect("tcp://127.0.0.1:%s" % 5559)
        self.outsocket.setsockopt(zmq.IDENTITY, "ms")

        self.digitalmixerpositions = {}
        self.digitalmixerresets = {}
        self.last_updates = {}
        self.scan_intervals = {}
        self.max_temps = {}
        self.last_outs_check = datetime.utcnow()

    def run(self):

        while True:

            run_loop = False
            for obj_id, ts in self.last_updates.items():
                if datetime.utcnow() > ts + timedelta(seconds=self.scan_intervals[obj_id]):
                    run_loop = True
                    break
            else:
                # run initially and at least every ten minutes
                if not len(self.last_updates) or (datetime.utcnow() > min(self.last_updates.values()) + timedelta(seconds=600)):
                    run_loop = True

            for ssn, vals in self.max_temps.items():
                if ssn is None:
                    continue
                val = memcache.get(ssn)
                if val and val > int(vals['max_temp']):
                    for _a in vals['ana_outs']:
                        self.outsocket.send_multipart(['set_output', str(_a), '0', str(max(vals['runtime']*1.5, 120)), '0'])
                    for _d in vals['dig_outs']:
                        self.outsocket.send_multipart(['set_output', str(_d), str(vals['runtime']*1.5), str(vals['runtime']*1.5), '0'])

            if not run_loop and not self.debug:
                # no scan_intervals exceeded
                time.sleep(1)
                continue

            else:

                mixer_ids = self._do_mixers()

                cc_ids = self._do_ccs()

                self._do_other_outs()

                l_u = dict((obj_id, ts.strftime("%Y-%m-%d %H:%M")) for obj_id, ts in self.last_updates.items() if obj_id in mixer_ids | cc_ids)

                try:
                    r = requests.post("http://localhost/json/v1/status/", json=l_u)
                except requests.ConnectionError:
                    pass

                if not len(l_u):
                    time.sleep(30)

            time.sleep(1)

    def _do_mixers(self):
        mixersreq = requests.get("http://localhost%s/json/v1/mixers/" % self.port)
        try:
            mixers = json.loads(mixersreq.content)
        except ValueError:
            return set()

        self.max_temps = {}  # re-initialize to remove possibly deleted sensors

        mixer_ids = set()

        for mixer in mixers:
            for mixer_id, vals in mixer.items():
                mixer_sid = "mixer_%s" % mixer_id

                self.scan_intervals[mixer_sid] = vals['scan_interval']

                if mixer_sid in self.last_updates and datetime.utcnow() < self.last_updates[mixer_sid] + timedelta(seconds=self.scan_intervals[mixer_sid]):
                    # again. the previous one just prevents unnecessary api calls.
                    continue

                if vals['output_id']:
                    mixer_ids.add(mixer_sid)

                    outputreq = requests.get("http://localhost%s/json/v1/outputs/%s/" % (self.port, vals['output_id']))
                    outputs = json.loads(outputreq.content)

                    _apin = outputs[0][str(vals['output_id'])]['analogpin']
                    _don = outputs[0][str(vals['output_id'])]['digital_on']
                    _doff = outputs[0][str(vals['output_id'])]['digital_off']

                    for _a in _apin:
                        self.outsocket.send_multipart(['set_output_mode', str(_a), 'analog'])
                    for dig in _don+_doff:
                        self.outsocket.send_multipart(['set_output_mode', str(dig), 'digital'])
                    for dn in _don:
                        for df in _doff:
                            self.outsocket.send_multipart(['add_dependent_outputs', str(dn), str(df)])

                    self.max_temps.setdefault(vals['max_temp_sensor'], dict())
                    self.max_temps[vals['max_temp_sensor']]['max_temp'] = vals['max_temp']
                    self.max_temps[vals['max_temp_sensor']]['ana_outs'] = _apin
                    self.max_temps[vals['max_temp_sensor']]['dig_outs'] = _doff
                    self.max_temps[vals['max_temp_sensor']]['runtime'] = vals['runtime']
                    
                    if vals.get('max_temp_sensor') is not None:
                        val = memcache.get(vals['max_temp_sensor'])
                    else:
                        val = None
                    if val and val > int(vals['max_temp']):
                        self.digitalmixerpositions[mixer_id] = 0
                    
                    else:
                        for _a in _apin:
                            self.outsocket.send_multipart(['set_output', str(_a), str(vals['position']), str(vals['scan_interval']), '2'])

                        if len(_don) and len(_doff):
                            if mixer_id not in self.digitalmixerpositions or mixer_id not in self.digitalmixerresets or self.digitalmixerresets[mixer_id] < datetime.utcnow()-timedelta(days=vals['calibration_interval']):
                                print "resetting pin %s for mixer %s" % (_doff, mixer_id)
                                self.digitalmixerresets[mixer_id] = datetime.utcnow()
                                self.digitalmixerpositions[mixer_id] = 0
                                for _d in _doff:
                                    self.outsocket.send_multipart(['set_output', str(_d), str(vals['runtime']*1.5), str(vals['runtime']*1.5), '0'])  # value & duration durch runtime, weil scan_interval kleiner sein kann als runtime

                            position = vals['runtime'] * (vals['position']/100.0)
                            if vals['position'] == 0 and self.digitalmixerpositions[mixer_id] > 0:
                                self.digitalmixerpositions[mixer_id] += 0.5*vals['runtime']  # hochsetzen, damit der mischer ueberfaehrt, weil unten die position abgezogen wird und hier ja 0 ist
                            elif vals['position'] == 100 and self.digitalmixerpositions[mixer_id] < vals['runtime']:
                                self.digitalmixerpositions[mixer_id] -= 0.5*vals['runtime']

                            if position < self.digitalmixerpositions[mixer_id]:
                                for _d in _doff:
                                    self.outsocket.send_multipart(['set_output', str(_d), str(self.digitalmixerpositions[mixer_id]-position), str(vals['scan_interval']), '2'])
                            elif position > self.digitalmixerpositions[mixer_id]:
                                for _d in _don:
                                    self.outsocket.send_multipart(['set_output', str(_d), str(position-self.digitalmixerpositions[mixer_id]), str(vals['scan_interval']), '2'])
                            else:  # ==
                                pass
                            self.digitalmixerpositions[mixer_id] = position

                        else:
                            pass

                    self.last_updates[mixer_sid] = datetime.utcnow()

        return mixer_ids

    def _do_ccs(self):
        ccyclesreq = requests.get("http://localhost%s/json/v1/differentialcontrols/" % self.port)
        try:
            ccycles = json.loads(ccyclesreq.content)
        except ValueError:
            return set()

        cc_ids = set()

        for ccycle in ccycles:
            for cc_id, vals in ccycle.items():
                cc_sid = "cc_%s" % cc_id

                self.scan_intervals[cc_sid] = vals['scan_interval']

                if cc_sid in self.last_updates and datetime.utcnow() < self.last_updates[cc_sid] + timedelta(seconds=self.scan_intervals[cc_sid]):
                    # see above
                    continue

                if vals["output_id"]:
                    cc_ids.add(cc_sid)

                    outputreq = requests.get("http://localhost%s/json/v1/outputs/%s/" % (self.port, vals['output_id']))
                    outputs = json.loads(outputreq.content)

                    for dp in outputs[0][str(vals['output_id'])]['digitalpin']:
                        self.outsocket.send_multipart(['set_output_mode', str(dp), 'digital'])
                        if vals['position'] > 0:
                            self.outsocket.send_multipart(['set_output', str(dp), str(vals['position']/100), '-1', '3'])
                        else:
                            self.outsocket.send_multipart(['set_output', str(dp), '0', str(vals['scan_interval']), '2'])

                    self.last_updates[cc_sid] = datetime.utcnow()

        return cc_ids

    def _do_other_outs(self):

        o = [_o for _o in self.scan_intervals.keys() if _o.startswith("out_")]
        if (len(o) and self.last_updates.get(o[0], datetime.utcnow()-timedelta(seconds=61)) + timedelta(seconds=60) < datetime.utcnow()) or (not len(o) and self.last_outs_check + timedelta(seconds=60) < datetime.utcnow()):
            self.last_outs_check = datetime.utcnow()
            ooutsreq = requests.get("http://localhost%s/json/v1/otherouts/" % self.port)
            try:
                oouts = json.loads(ooutsreq.content)
            except ValueError:
                return

            for mod, vals in oouts.items():
                for ausgangno, val in vals.items():
                    try:
                        if not (ausgangno.isdigit() and (isinstance(val, int) or isinstance(val, float))):
                            continue
                    except AttributeError:
                        continue
                    self.scan_intervals["out_%s" % ausgangno] = 60
                    self.last_updates["out_%s" % ausgangno] = datetime.utcnow()
                    if 0 < int(ausgangno) < 13:
                        self.outsocket.send_multipart(['set_output_mode', str(ausgangno), 'digital'])
                        if int(val) > 0:
                            self.outsocket.send_multipart(['set_output', str(ausgangno), '1', '-1', '3'])
                        elif int(val) == 0:
                            self.outsocket.send_multipart(['set_output', str(ausgangno), '0', '1', '2'])
                    elif 13 <= int(ausgangno) <= 14:
                        self.outsocket.send_multipart(['set_output_mode', str(ausgangno), 'analog'])
                        self.outsocket.send_multipart(['set_output', str(ausgangno), str(val), '-1', '2'])

if __name__ == "__main__":
    ms = Mischersteuerung()
    # import sys
    try:
        ms.run()
    except Exception as e:
        import sys, requests, traceback, json
        from uuid import getnode
        try:
            typ, value, trace = sys.exc_info()
            ret = {
                'location': 'outputcontrol/outputcontrol.py',
                'exc_type': typ.__name__,
                'exc_msg': typ.__doc__,
                'exc_value': str(value).translate(None, "'").translate(None, '"'),
                'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
            }
            enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
            err = enc
            macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
        except:
            pass
