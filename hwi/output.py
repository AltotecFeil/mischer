import logging
from datetime import datetime, timedelta
import zmq
from zmq.eventloop.ioloop import IOLoop, PeriodicCallback
from zmq.eventloop.zmqstream import ZMQStream
import wiringpi
import time
import smbus
import requests


class HWI:

    def __init__(self, loop, zmq_insocket):
        wiringpi.wiringPiSetup()
        wiringpi.mcp23017Setup(64, 0x21)
        # keys are the numbers for the outputs on the master miniserver
        self.out2pin = {1: 73, 2: 74, 3: 75, 4: 76, 5: 77, 6: 65, 7: 66, 8: 67, 9: 68, 10: 69, 11: 71, 12: 70, 13: 0x02, 14: 0x04}
        for pin in self.out2pin.values():
            wiringpi.pinMode(pin, 1)

        wiringpi.pinMode(78, 0)
        wiringpi.pullUpDnControl(78, 2)
        wiringpi.pinMode(79, 0)
        wiringpi.pullUpDnControl(79, 2)

        self._smbus = smbus.SMBus(1)
        self._smbus.write_byte_data(0x61, 0x01, 0x00)
        self._smbus.write_byte_data(0x61, 0x02, 0xff)
        self._smbus.write_byte_data(0x61, 0x03, 0x00)
        self._smbus.write_byte_data(0x61, 0x04, 0xff)
        self._smbus.write_byte_data(0x61, 0x05, (0x02 << 0) | (0x03 << 2))  # 0xfa)

        self._queue = {}
        self._inputs = {}
        self._dependent_outputs = {}  # if one is 1, the other needs to be 0
        self._output_modes = {}

        self.loop = loop
        self.insocket = ZMQStream(zmq_insocket)
        self.insocket.on_recv(self.handle_command)

        self.runner = PeriodicCallback(self.run, 1000, self.loop)
        self.runner.start()

    def handle_command(self, msg):
        print "%s - received message: %s" % (datetime.utcnow(), msg)
        getattr(self, msg[1])(*msg[2:])

    def add_dependent_outputs(self, pin1, pin2):
        self._dependent_outputs[pin1] = pin2
        self._dependent_outputs[pin2] = pin1

    def remove_dependent_outputs(self, pin1, pin2):
        try:
            del self._dependent_outputs[pin1]
        except KeyError:
            pass
        try:
            del self._dependent_outputs[pin2]
        except KeyError:
            pass

    def set_output_mode(self, pin, mode):
        if mode not in {'analog', 'digital'}:
            return False
        self._output_modes[pin] = mode

    def set_output(self, pin, value, duration=0, priority=-1):
        if self._output_modes.get(pin):
            self._queue.setdefault(pin, list())
            if priority == '0' and len(self._queue[pin]):  # notfall
                if self._queue[pin][0][1] > '0':
                    self._queue[pin][0] = (value, priority, datetime.utcnow(), None, duration)
            else:
                self._queue[pin].append((value, priority, datetime.utcnow(), None, duration))

    def _set_digital_output(self, pin, value):
        wiringpi.digitalWrite(pin, value)
        time.sleep(0.05)

    def _set_analog_output(self, pin, value):
        value = 255.0 - 255.0 * (float(value)/100.0)
        self._smbus.write_byte_data(0x61, pin, int(value))
        time.sleep(0.05)

    def run(self):

        try:
            #for pin, (value, priority, created, started, duration) in self._queue.items():
            for pin, pinqueue in self._queue.items():
                
                if len(pinqueue):
                
                    mode = self._output_modes.get(pin)
                    value, priority, created, started, duration = pinqueue[0]

                    if mode == 'analog':
                        
                            if started is None:
                                self._set_analog_output(self.out2pin[int(pin)], value)
                            del self._queue[pin][0]

                    elif mode == 'digital':

                        if started is not None:  # nur mischerausgaenge

                            if (started + timedelta(seconds=float(value))) > datetime.utcnow():
                                pass  # noch nicht abgelaufen
                            else:
                                self._set_digital_output(self.out2pin[int(pin)], 0)
                                del self._queue[pin][0]

                        else:

                            if duration == '-1':  # dauerhaft geschalteter ausgang
                                self._set_digital_output(self.out2pin[int(pin)], int(value))
                                del self._queue[pin][0]

                            else:  # mischerausgang
                                if pin in self._dependent_outputs and len(self._queue.get(self._dependent_outputs[pin], [])):
                                    if self._queue[self._dependent_outputs[pin]][0][3] is not None:  # dependent_output faehrt grade
                                        if priority < self._queue[self._dependent_outputs[pin]][0][1]:
                                            del self._queue[self._dependent_outputs[pin]][0]
                                            self._set_digital_output(self.out2pin[int(self._dependent_outputs[pin])], 0)
                                            started = datetime.utcnow()
                                            self._queue[pin][0] = (value, priority, created, started, duration)
                                            self._set_digital_output(self.out2pin[int(pin)], 1 if float(value) > 0 else 0)
                                        else:
                                            pass  # warten
                                    else:
                                        if created < self._queue[self._dependent_outputs[pin]][0][2]:  # war zuerst da
                                            started = datetime.utcnow()
                                            self._queue[pin][0] = (value, priority, created, started, duration)
                                            self._set_digital_output(self.out2pin[int(pin)], 1 if float(value) > 0 else 0)
                                        else:
                                            pass  # der dependent_output sollte fahren

                                else:  # dependent_output hat gerade nichts zu tun
                                    started = datetime.utcnow()
                                    self._queue[pin][0] = (value, priority, created, started, duration)
                                    self._set_digital_output(self.out2pin[int(pin)], 1 if float(value) > 0 else 0)

            if datetime.utcnow().second % 10 == 0:
                ui1value = not wiringpi.digitalRead(79)
                ui2value = not wiringpi.digitalRead(78)
                self._inputs['ui1'] = {'name': 'ui1', 'value': ui1value}
                self._inputs['ui2'] = {'name': 'ui2', 'value': ui2value}
                requests.post("http://localhost/json/v1/inputs/", json=self._inputs)

        except Exception:
            logging.exception("exc in run")
            self.loop.stop()

            import sys, traceback, json
            from uuid import getnode
            from fabric.api import local
            try:
                typ, value, trace = sys.exc_info()
                ret = {
                    'location': 'hwi/output.py',
                    'exc_type': typ.__name__,
                    'exc_msg': typ.__doc__,
                    'exc_value': str(value).translate(None, "'").translate(None, '"'),
                    'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
                }
                enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
                err = enc
                macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
            except Exception:
                logging.exception("exc sending exc")


if __name__ == "__main__":

    context = zmq.Context()
    insocket = context.socket(zmq.ROUTER)
    insocket.bind("tcp://127.0.0.1:%s" % 5559)

    loop = IOLoop.current()
    _hwi = HWI(loop, insocket)
    
    loop.start()
