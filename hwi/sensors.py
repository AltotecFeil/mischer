from pyownet import protocol
import requests
import time
from fabric.api import local, hide
import smbus

bus = smbus.SMBus(1)


try:
    owproxy = protocol.proxy(flags=protocol.FLG_FORMAT_FIC | protocol.FLG_TEMP_C)
except (protocol.ConnError, protocol.ProtocolError):
    local("sudo service owserver restart")
    local("sudo service owfs restart")
    
    import sys, traceback, json
    from uuid import getnode
    try:
        typ, value, trace = sys.exc_info()
        ret = {
            'location': 'hwi/sensors.py',
            'exc_type': typ.__name__,
            'exc_msg': typ.__doc__,
            'exc_value': str(value).translate(None, "'").translate(None, '"'),
            'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
        }
        enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
        err = enc
        macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
        requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
    except:
        pass
# except protocol.ProtocolError:
#     pass

else:

    sensors = {}
    while True:

        with hide("output"):
            try:
                local("sudo service owserver status | grep 'Active: active'")
            except:
                local("sudo service owserver restart")

            try:
                local("sudo service owfs status | grep 'Active: active'")
            except:
                local("sudo service owfs restart")

        try:
            sensors = {}
            for i in range(8):
                try:
                    sdirs = owproxy.dir('/bus.%s/' % i)
                    sob = [sdir for sdir in sdirs if sdir[:9] in {'/bus.%s/10' % i, '/bus.%s/28' % i, '/bus.%s/26' % i}]
                    for s in sob:
                        ssn = owproxy.read(s + 'address')
                        sensors[ssn] = {'linie': i}
                        val = owproxy.read(s + 'temperature')
                        try:
                            sensors[ssn]['value'] = float(val)
                        except (ValueError, TypeError):
                            del sensors[ssn]
                except protocol.OwnetError:
                    continue

            if not len(sensors):
                local("sudo python /home/pi/mischer/hwi/gpio13hilo.py")

            cfg = 0b00011100
            vals = []
            for i in range(20):
                try:
                    ret = bus.read_i2c_block_data(0x69, cfg, 4)
                except IOError:
                    try:
                        ret = bus.read_i2c_block_data(0x68, cfg, 4)
                    except IOError:
                        continue

                if ret[3] == cfg:
                    if ret[0] & 0x80:
                        wert = (((ret[0] ^ 0xff) & 0x7f) << 16) + ((ret[1] ^ 0xff) << 8) + (ret[2] ^ 0xff)
                        wert = wert * (-1)
                    else:
                        wert = ((ret[0]) << 16) + (ret[1] << 8) + ret[2]

                    vals.append(15.625*wert/1000)
                time.sleep(0.3)
            if len(vals) < 10:
                ret = None
            else:
                vals = sorted(vals)[len(vals)/4:-len(vals)/4]
                ret = sum(vals)/len(vals)

            sensors['pt1000'] = {'linie': -1, 'value': ret}
            
            requests.post("http://localhost/json/v1/sensors/", json=sensors)

        except protocol.OwnetError:
            import sys, traceback, json
            from uuid import getnode
            try:
                typ, value, trace = sys.exc_info()
                ret = {
                    'location': 'hwi/sensors.py',
                    'exc_type': typ.__name__,
                    'exc_msg': typ.__doc__,
                    'exc_value': str(value).translate(None, "'").translate(None, '"'),
                    'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)],
                    'extra_msg': "restarting the process now"
                }
                if ret['exc_value'] != '[Errno 5] legacy - IO error: /':  # /mnt/1wire leer
                    enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
                    err = enc
                    macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
                    requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
            except:
                pass

            # lets be bad:
            # sys.exit(0)
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart sensors")

            # pass
        # oder doch?! und dann ein sys.exit, damit wir oben in die fehlerbehandlung reinlaufen?

        except Exception as e:
            import sys, traceback, json
            from uuid import getnode
            try:
                typ, value, trace = sys.exc_info()
                ret = {
                    'location': 'hwi/sensors.py',
                    'exc_type': typ.__name__,
                    'exc_msg': typ.__doc__,
                    'exc_value': str(value).translate(None, "'").translate(None, '"'),
                    'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(trace)]
                }
                enc = local("""python -c "from services.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
                err = enc
                macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
            except:
                pass

        if len(sensors) >= 10:
            time.sleep(1)
        else:
            time.sleep(10-len(sensors))
