import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(13, GPIO.OUT)
GPIO.output(13, GPIO.HIGH)
time.sleep(1)
GPIO.output(13, GPIO.LOW)

GPIO.setup(24, GPIO.OUT)
GPIO.output(24, GPIO.HIGH)
time.sleep(1)
GPIO.output(24, GPIO.LOW)

GPIO.cleanup()
